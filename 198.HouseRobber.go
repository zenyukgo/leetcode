func Max(x, y int) int {
    if x > y {
        return x
    } else {
        return y
    }
}

func rob(nums []int) int {
    length := len(nums)
    if length == 0 {
        return 0
    }
    if length == 1 {
        return nums[0]
    }
    if length == 2 {
        return Max(nums[0], nums[1])
    }
    var dpMax = make([]int, length)
    dpMax[0] = nums[0]
    dpMax[1] = Max(nums[0], nums[1])
    for i := 2; i < length; i++ {
        dpMax[i] = Max(nums[i] + dpMax[i-2], dpMax[i-1])
    }
    return dpMax[length - 1]
}