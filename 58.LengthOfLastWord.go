func lengthOfLastWord(s string) int {
    if  len(s) == 0 {
        return 0
    }
    s = strings.TrimRight(s, " ")
    lastSpaceIdx := strings.LastIndex(s, " ")
    if lastSpaceIdx == -1 {
        return len(s)
    }
    return len(s[lastSpaceIdx+1:])
}