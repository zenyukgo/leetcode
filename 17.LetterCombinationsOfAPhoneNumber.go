func letterCombinations(digits string) []string {
    result := []string{}
    dToLetters := map[rune][]rune{
        '2': {'a', 'b', 'c'},
        '3': {'d', 'e', 'f'},
        '4': {'g', 'h', 'i'},
        '5': {'j', 'k', 'l'},
        '6': {'m', 'n', 'o'},
        '7': {'p', 'q', 'r', 's'},
        '8': {'t', 'u', 'v'},
        '9': {'w', 'x', 'y', 'z'},
    }

    for _, d := range digits {
        result = addLetters(dToLetters[d], result) 
    }
    return result
}

func addLetters(letters []rune, intermediate []string) (result []string) {
    if len(intermediate) == 0 {
        for _, l := range letters {
            result = append(result, string(l))
        }
        return
    }
    for _, l := range letters {
        for _, i := range intermediate {
            result = append(result, i + string(l))
        }
    }
    return
}