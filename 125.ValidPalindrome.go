func isPalindrome(s string) bool {
    l := 0
    h := len(s) - 1
    lowRune := ' '
    highRune := ' '
    for l < h {
        lowRune, l = nextFromFront(s, l, h)
        highRune, h = nextFromEnd(s, h, l)
        if lowRune != highRune {
            return false
        }
        l++
        h--
    }
    return true
}

func nextFromFront(s string, pos int, limit int) (rune, int) {
    r := rune(s[pos])
    for !isAlphanumeric(r) && pos < limit {
        pos++
        r = rune(s[pos])
    }
    r = toLower(r)
    return r, pos
}

func nextFromEnd(s string, pos int, limit int) (rune, int) {
    r := rune(s[pos])
    for !isAlphanumeric(r) && pos > limit {
        pos--
        r = rune(s[pos])
    }
    r = toLower(r)
    return r, pos
}

func toLower(r rune) rune {
    if r >= 65 && r <= 90 {
        r = rune(r + 32)
    }
    return r
}

func isAlphanumeric(r rune) bool {
    i := int(r)
    return (i >= 65 && i <= 90) || (i >= 97 && i <= 122) || (i >= 48 && i <= 57)
}