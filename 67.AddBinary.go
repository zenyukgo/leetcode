func addBinary(a string, b string) string {
    short := b
    long := a
    if len(a) < len(b) {
        short = a
        long = b
    }
    result := ""
    plusOne := false
    diff := len(long) - len(short)
    for i := len(long) - 1; i >= 0; i-- {
        shortPos := i - diff
        if shortPos < 0 {
            if plusOne {
                if long[i] == '1' {
                    result = "0" + result    
                } else {
                    result = "1" + result
                    plusOne = false
                }
            } else {
                result = string(long[i]) + result
            }
            continue
        }
        if short[i - diff] == '1' && long[i] == '1' && plusOne {
            result = "1" + result
        } else if (short[i - diff] == '1' && long[i] == '1') || (short[i - diff] == '1' && plusOne) || (long[i] == '1' && plusOne) {
            plusOne = true
            result = "0" + result
        } else if (short[i - diff] == '1' || long[i] == '1' || plusOne) {
            plusOne = false
            result = "1" + result
        } else {
            plusOne = false
            result = "0" + result
        }
    }
    if plusOne {
        result = "1" + result
    }
    return result
}