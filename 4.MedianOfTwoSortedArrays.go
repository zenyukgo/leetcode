func findMedianSortedArrays(nums1 []int, nums2 []int) float64 {
    // go through elements from both input slices
    // by taking one from begging, either from 1st or 2nd
    // depending on which one is lower   
    index1, index2 := 0, 0
    count := 1
    length1 := len(nums1)
    length2 := len(nums2)
    totalLength := length1 + length2
    totalIsEven := totalLength % 2 == 0
    tempItem := 0
    for count <= (totalLength + 1) / 2 {
        if (index2 >= length2) {
            tempItem = nums1[index1]
            index1++
        } else if (index1 >= length1) {
            tempItem = nums2[index2]
            index2++
        } else if nums1[index1] < nums2[index2] {
            tempItem = nums1[index1]
            index1++
        } else {
            tempItem = nums2[index2]
            index2++
        }
        count++
    }
    var result float64
    result = float64(tempItem)
    if totalIsEven {
        secondPartOfMedian := 0
        if (index2 >= length2) {
            secondPartOfMedian = nums1[index1]
        } else if (index1 >= length1) {
            secondPartOfMedian = nums2[index2]
        } else if nums1[index1] < nums2[index2] {
            secondPartOfMedian = nums1[index1]
        } else {
            secondPartOfMedian = nums2[index2]
        }
        result = (result + float64(secondPartOfMedian)) / 2
    }
    return result
}