func searchInsert(nums []int, target int) int {
    pos := -1
    n := 0
    for pos, n = range nums {
        if n >= target {
            return pos
        } 
    }
    return pos + 1
}