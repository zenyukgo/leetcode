func uniquePaths(m int, n int) int {
    matrix := make([][]int, m)
    for i := 0; i < m; i++ {
        matrix[i] = make([]int, n)
        for j := 0; j < n; j++ {
            if i == 0 || j == 0 {
                matrix[i][j] = 1
                continue
            }
            matrix[i][j] = matrix[i-1][j] + matrix[i][j-1]
        }    
    }
    return matrix[m-1][n-1]
}

/*
11111111111
123456
136

*/