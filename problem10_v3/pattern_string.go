package problem10

import (
	"strings"
)

type PatternString []PatternChar

func (s PatternString) String() string {
	sb := strings.Builder{}
	sb.WriteString("pattern: ")
	for _, c := range s {
		sb.WriteString(c.String())
	}
	sb.WriteRune('\n')
	return sb.String()
}
