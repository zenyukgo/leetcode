package problem10

import (
	"fmt"
)

func isMatch(s string, p string) bool {
	defer println()
	pattern := findStarredChars(p)
	decomposed := NewDecomposedPatternString(pattern)
	for _, r := range decomposed.Singles {
		fmt.Printf("%v", string(r))
	}

	matched := applyPattern(s, decomposed)
	if !matched {
		return false
	}

	// check match between blocks of strings leftovers and set of corresponding patterns
	//for i := 0; i < len(blocksOfStringsNotSimplyMatched); i++ {
	//	if !stringMatches(blocksOfStringsNotSimplyMatched[i], decomposed.SequentialBlocksOfRepetitive[i]) {
	//		return false
	//	}
	//}
	return true
}

func stringMatches(s []rune, pattern PatternString) bool {
	index := 0
	for _, p := range pattern {
		if p.char == s[index] {
			index++
			continue
		}
		// TODO: add cases with dot (.) -> must have a char; star (*)
	}
	return true
}

func applyPattern(s string, decomposed DecomposedPatternString) bool {
	// each non-repetitive char from the pattern should have a match in the search string

	var blocksOfStringsNotSimplyMatched [][]rune // not matched by a simple match, like a = a

	// debug
	for i, block := range decomposed.SequentialBlocksOfRepetitive {
		fmt.Printf("\n repetitive %d: \n", i)
		for _, repetitiveChar := range block {
			fmt.Printf("%v", repetitiveChar)
		}
	}
	println()

	if len(decomposed.Singles) > 0 {
		var matched bool
		matched, blocksOfStringsNotSimplyMatched = applySingles(s, decomposed)
		if !matched {
			return false
		}
	} else {
		return true
	}

	// debug
	for i, block := range blocksOfStringsNotSimplyMatched {
		fmt.Printf("\n leftover %d: \n", i)
		for _, c := range block {
			fmt.Printf("%v", string(c))
		}
	}
	println()

	// pattern is missing corresponding blocks
	if len(blocksOfStringsNotSimplyMatched) > len(decomposed.SequentialBlocksOfRepetitive) {
		return false
	}

	return true
}

func applySingles(s string, decomposed DecomposedPatternString) (matched bool, blocksOfStringsNotSimplyMatched [][]rune) {
	startIndex := 0
	stringInRunes := []rune(s)

out:
	for _, p := range decomposed.Singles {
		fmt.Printf("\n s=%v; p=%v", string(stringInRunes[startIndex:]), string(p))
		var block []rune
		for i := startIndex; i < len(s); i++ {
			if p == stringInRunes[i] || p == '.' {
				startIndex = i + 1
				if len(block) > 0 {
					blocksOfStringsNotSimplyMatched = append(blocksOfStringsNotSimplyMatched, block)
				}
				block = []rune{}
				fmt.Printf("\n done p=%v", string(p))
				continue out
			} else {
				block = append(block, stringInRunes[i])
			}
		}
		fmt.Printf("\n done p=%v", string(p))
		return false, blocksOfStringsNotSimplyMatched
	}
	if startIndex < len(stringInRunes) {
		return false, blocksOfStringsNotSimplyMatched
	}
	return true, blocksOfStringsNotSimplyMatched
}

func findStarredChars(pattern string) (result PatternString) {
	var previous PatternChar
	for i, r := range []rune(pattern) {
		if r == '*' && i == 0 {
			continue
		}
		if r == '.' {
			previous = PatternChar{true, '.', false}
			result = append(result, previous)
			continue
		}
		if r == '*' {
			previous.repeated = true
			result[len(result)-1] = previous
			continue
		}
		if previous.repeated && previous.char == r {
			// skipping as we already have this char in sequence
			// merge repeated sequence with the same single following char, e.g. "a*a" -> "a*"
			continue
		}
		previous = PatternChar{false, r, false}
		result = append(result, previous)
	}
	return
}
