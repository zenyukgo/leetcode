package problem10

// DecomposedPatternString represents a string in a pattern search usable format
// Example: "ab*cd*e" -> a, c, e - sequence of singles; b*, d* - sequence of repeating chars
type DecomposedPatternString struct {
	OriginalPattern              PatternString
	Singles                      []rune
	SequentialBlocksOfRepetitive []PatternString
	DotIndexes                   []int // indexes of special - dot (.) characters representing any character
}

func NewDecomposedPatternString(pattern PatternString) (result DecomposedPatternString) {
	result.OriginalPattern = pattern
	var block PatternString
	for i, c := range pattern {
		if c.anyChar {
			result.DotIndexes = append(result.DotIndexes, i)
		}
		if !c.repeated {
			result.Singles = append(result.Singles, c.char)
			if len(block) > 0 {
				result.SequentialBlocksOfRepetitive = append(result.SequentialBlocksOfRepetitive, block)
			}
			block = PatternString{}
		} else {
			block = append(block, c)
		}
	}
	return
}
