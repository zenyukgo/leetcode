package problem10

import (
	"fmt"
)

type PatternChar struct {
	anyChar  bool
	char     rune
	repeated bool
}

func (r PatternChar) String() string {
	if r.repeated {
		return fmt.Sprintf("char: %s repeated\n", string(r.char))
	}
	return fmt.Sprintf("char: %s\n", string(r.char))
}
