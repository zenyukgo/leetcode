func longestPalindrome(s string) string {
    // using sliding window
    input := []rune(s)
    inputLength := len(s)
    windowSize := len(s)
    for windowSize > 1 {
        begining := 0
        for begining + windowSize <= inputLength {
            toBeChecked := input[begining:begining + windowSize]
            if checkForPalindrom(toBeChecked) {
                return string(toBeChecked)
            }
            // move window
            begining++
        }
        // squeeze window for next run
        windowSize--
    }
    if inputLength > 0 {
        return string([]rune(s)[0])
    } 
    return ""
}

func checkForPalindrom(s []rune) bool {
    length := len(s)
    for i := 0; i < (length / 2); i++ {
        if s[i] != s[length - i - 1] {
            return false
        } 
    }
    return true
}