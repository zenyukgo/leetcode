func plusOne(digits []int) []int {
    plusOne := false
    for i := len(digits) - 1; i >= 0; i-- {
        if digits[i] == 9 {
            plusOne = true
            digits[i] = 0
        } else {
            plusOne = false
            digits[i] += 1
            break
        }
    }
    if plusOne {
        digits = append([]int{1}, digits...)
    }
    return digits
}