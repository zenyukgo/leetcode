func isValid(s string) bool {
    if len(s) == 1 {
        return false
    }
    stack := []rune{}
    all := map[rune]struct{}{
        '(': struct{}{},
        ')': struct{}{},
        '[': struct{}{},
        ']': struct{}{},
        '{': struct{}{},
        '}': struct{}{},
    }
    openning := map[rune]struct{}{
        '(': struct{}{},
        '[': struct{}{},
        '{': struct{}{},
    }
    for _, r := range s {
        _, isParen := all[r]
        if isParen {
            _, isOpening := openning[r]
            if isOpening {
                stack = append(stack, r)
                continue
            }
            // expection only closing ones from here
            if len(stack) == 0 {
                return false
            }
            opening := getOpenning(r)
            if stack[len(stack)-1:][0] == opening {
                stack = stack[:len(stack)-1]
            } else {
                stack = append(stack, r)
            }
        }
    }
    return len(stack) == 0
}

func getOpenning(closing rune) (result rune) {
    if closing == ')' {
        result = '('
    } else if closing == ']' {
        result = '['
    } else {
        result = '{'
    }
    return
}