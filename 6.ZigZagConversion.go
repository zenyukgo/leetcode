type row []rune

func convert(s string, numRows int) string {
    if numRows <= 1 {
        return s
    }
    rows := make([]row, numRows)
    for i := 0; i < numRows; i++ {
        rows[i] = []rune{}
    }
    forward := false
    currentRow := 0
    for _, ch := range []rune(s) {
        rows[currentRow] = append(rows[currentRow], ch)
        if forward {
            if currentRow + 1 == numRows {
                forward = false
                currentRow--
            } else {
                currentRow++
            } 
        } else {
            if currentRow == 0 { 
                forward = true
                currentRow++
            } else {
                currentRow--
            } 
        }
    }
    return readRows(rows, len(s))
}

func readRows(rows []row, length int) string {
    result := make([]rune, length)
    index := 0
    for i := 0; i < len(rows); i++ {
        for _, ch := range rows[i] {
            result[index] = ch
            index++
        }
    }
    return string(result)
}