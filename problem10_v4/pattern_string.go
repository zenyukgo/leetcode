package problem10

import (
	"strings"
)

type PatternString []PatternChar

func NewPatternString(pattern string) (result PatternString) {
	var previous PatternChar
	for i, r := range []rune(pattern) {
		if r == '*' && i == 0 {
			continue
		}
		if r == '.' {
			previous = PatternChar{true, '.', false}
			result = append(result, previous)
			continue
		}
		if r == '*' {
			previous.repeated = true
			result[len(result)-1] = previous
			continue
		}
		previous = PatternChar{false, r, false}
		result = append(result, previous)
	}
	return
}


func (s PatternString) String() string {
	sb := strings.Builder{}
	sb.WriteString("pattern:\n")
	for _, c := range s {
		sb.WriteString(c.String())
	}
	return sb.String()
}
