// ! Array’s length is part of its type, hence can't reduce the underlying array, also in condition - one can't create a new array or slice

func removeDuplicates(nums []int) int {
    if len(nums) < 2 {
        return len(nums)
    }
    result := 1
    slow := 0
    fast := 1
    for fast < len(nums) {
        if nums[slow] == nums[fast] {
            fast++
        } else {
            nums[slow+1] = nums[fast]
            //todo: not possible to return array of diff. size
            slow++
            fast++
        }
    }
    return result
}