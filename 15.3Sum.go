func threeSum(nums []int) [][]int {
    result := map[string][]int{}
    
    // key is the number in original 'nums', value is it's index index
    all := map[int][]int{}
    for idx, n := range nums {
        all[n] = append(all[n], idx)
    }
    length := len(nums)
    for i := 0; i < length; i++ {
        for j := i + 1; j < length; j++{
            sumOfTwo := (nums[i] + nums[j]) * -1
            elements, ok := all[sumOfTwo]
            if ok {
                for _, index := range elements {
                    // don't use same one twice in same set
                    if index == i || index == j {
                        continue
                    }
                    r := []int{nums[i], nums[j], sumOfTwo}
                    sort.Ints(r)
                    // create hash
                    key := strconv.Itoa(r[0]) + strconv.Itoa(r[1]) + strconv.Itoa(r[2])
                    result[key] = r
                    break
                }
            }
        }
    }
    // format output
    ready := [][]int{}
    for _, v := range result {
        ready = append(ready, v)
    }
    return ready
}