func countAndSay(n int) string {
    return next("1", 1, n)
}

func next(str string, count int, stop int) string {
    if count >= stop {
        return str
    }
    result := ""
    for {
        count := 0
        repeat := ' '
        ok := false
        count, repeat, str, ok = countRepeatedLeft(str)
        if !ok {
            break
        }
        result += generateStr(repeat, count)
    }
    return next(result, count+1, stop)
}

func countRepeatedLeft(str string) (count int, repeat rune, restOfStr string, ok bool) {
    if len(str) == 0 {
        return 
    }
    repeat = rune(str[0])
    count = 1
    ok = true
    for _, s := range str[1:] {
        if s == repeat {
            count++
        } else {
            break
        }
    }
    restOfStr = str[count:]
    return
}

func generateStr(r rune, count int) (result string) {
    result = strconv.Itoa(count)
    result += string(r)
    return
}