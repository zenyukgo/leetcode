func generateParenthesis(n int) []string {
    return generateSub(n)
}

func generateSub(n int) []string {
    if n == 1 {
        return []string{"()"}
    }
    sub := generateSub(n - 1)
    result := []string{}
    for _, s := range sub {
        result = append(result, "(" + s + ")")
        result = append(result, "()" + s)
        if "()" + s != s + "()" {
            result = append(result, s + "()")
        }
    }
    return result
}