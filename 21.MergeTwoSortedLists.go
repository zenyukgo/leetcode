/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */

func mergeTwoLists(l1 *ListNode, l2 *ListNode) *ListNode {
    if l1 == nil {
        return l2
    }
    if l2 == nil {
        return l1
    }
    current := &ListNode {
        Val: l1.Val,
    }
    if l2.Val < l1.Val {
        current.Val = l2.Val
        l2 = l2.Next
    } else {
        l1 = l1.Next
    }
    result := current
    for l1 != nil && l2 != nil {
        current.Next = &ListNode{
            Val: l1.Val,
        }
        if l2.Val < l1.Val {
            current.Next.Val = l2.Val
            l2 = l2.Next
        } else {
            l1 = l1.Next
        }
        current = current.Next
    }
    if l1 != nil {
        current.Next = l1
    }
    if l2 != nil {
        current.Next = l2
    }
    return result
}