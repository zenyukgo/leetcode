func fourSum(nums []int, target int) [][]int {
    result := [][]int{}
    length := len(nums)
    if length < 4 {
        return result
    }
    sort.Ints(nums)
    added := map[string]struct{}{}
    for i := 0; i < length; i++ {
        for j := i + 1; j < length; j++ { 
            l := j + 1
            r := length - 1
            for l < r {
                sum := nums[i] + nums[j] + nums[l] + nums[r]
                if sum == target {
                    hash := strconv.Itoa(nums[i]) + strconv.Itoa(nums[j]) + strconv.Itoa(nums[l]) + strconv.Itoa(nums[r])
                    _, ok := added[hash]
                    if !ok {
                        result = append(result, []int{nums[i], nums[j], nums[l], nums[r]})
                        added[hash] = struct{}{}
                    }
                    l++
                    r--
                } else if sum < target {
                    l++
                } else {
                    r--
                }
            }
        }
    }
    return result       
}