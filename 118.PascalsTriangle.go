func generate(numRows int) [][]int {
    if numRows < 1 {
        return [][]int{}
    }
    result := [][]int{[]int{1}}
    for i := 1; i < numRows; i++ {
        next := nextRow(result[i-1])
        result = append(result, next)
    }
    return result
}

func nextRow(prev []int) []int {
    length := len(prev) + 1
    result := make([]int, length)
    result[0] = 1
    result[length-1] = 1
    for i := 1; i < length/2; i++ {
        result[i] = prev[i-1] + prev[i]
    }
    for i := length/2; i < length - 1; i++ {
        result[i] = prev[i-1] + prev[i]
    }
    odd := length % 2 != 0
    if odd {
        result[length/2] = prev[length/2 - 1] + prev[length/2]
    }
    return result
}
/*

1
11
121
1331
14641
150051

*/