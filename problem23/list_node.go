package problem23

import (
	"strconv"
	"strings"
)

// ListNode Definition for singly-linked list.
type ListNode struct {
	Val int
	Next *ListNode
}

func (l *ListNode) String() string {
	//todo: go all the way to the end
	var builder strings.Builder
	current := l
	builder.WriteString("[")
	for current != nil {
		builder.WriteString(strconv.Itoa(current.Val))
		builder.WriteString(" ")
		current = current.Next
	}
	builder.WriteString("]")
	return builder.String()
}
