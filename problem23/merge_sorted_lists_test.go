package problem23

import (
	"fmt"
	"testing"
)

func TestMergeKLists(t *testing.T) {
	lists := generateTestLists()
	fmt.Printf("lists: %v\n", lists)

	merged := mergeKLists(lists)
	fmt.Printf("merged: %v\n", merged)
}

func generateTestLists() []*ListNode {
	var result []*ListNode

	unitsThree := &ListNode{333, nil}
	unitsTwo := &ListNode{22, unitsThree}
	unitsOne := &ListNode{1, unitsTwo}
	result = append(result, unitsOne)

	tensThree := &ListNode{304, nil}
	tensTwo := &ListNode{222, tensThree}
	tensOne := &ListNode{10, tensTwo}
	result = append(result, tensOne)

	hundredsThree := &ListNode{303, nil}
	hundredsTwo := &ListNode{204, hundredsThree}
	hundredsOne := &ListNode{100, hundredsTwo}
	result = append(result, hundredsOne)

	return result
}
