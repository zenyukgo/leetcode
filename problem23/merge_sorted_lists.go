package problem23

import (
	"math"
)

func mergeKLists(lists []*ListNode) *ListNode {
	var result *ListNode
	var resultTail *ListNode
	for {
		smallestValue := math.MaxInt
		listIndex := -1
		// compare first items of each list
		for i, n := range lists {
			if n == nil {
				continue
			}
			if n.Val < smallestValue {
				smallestValue = n.Val
				listIndex = i
			}
		}
		if listIndex < 0 {
			break
		}
		smallestNode := lists[listIndex]

		if result == nil {
			// set head only once
			result = smallestNode

			resultTail = smallestNode
		} else {
			// point last one to the new next one
			resultTail.Next = smallestNode
			resultTail =  smallestNode
		}
		// shift list with removed item
		lists[listIndex] = smallestNode.Next
	}
	return result
}