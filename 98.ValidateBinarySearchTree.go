/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */

const MaxUInt = ^uint(0)
const MaxInt = int(MaxUInt >> 1)
const MinInt = -MaxInt -1

func isValidBST(root *TreeNode) bool {
    if root == nil {
        return true
    }
    left := true
    if root.Left != nil {
        if root.Val <= root.Left.Val {
            return false
        }
        left = isValidSub(root.Left, MinInt, root.Val)
    }
    right := true
    if root.Right != nil {
        if root.Val >= root.Right.Val {
            return false
        }
        right = isValidSub(root.Right, root.Val, MaxInt)
    }
    return left && right
}

func isValidSub(node *TreeNode, l int, h int) bool {
    if node.Left != nil {
        if node.Left.Val >= node.Val || node.Left.Val >= h || node.Left.Val <= l {
            return false
        }
        valid := isValidSub(node.Left, l, node.Val)
        if !valid {
            return false
        }
    }
    if node.Right != nil {
        if node.Right.Val <= node.Val || node.Right.Val >= h || node.Right.Val <= l {
            return false
        }
        valid := isValidSub(node.Right, node.Val, h)
        if !valid {
            return false
        }
    }
    return node.Val > l && node.Val < h
}

/*

       5
    1     4
         3 6
------------------         
         
        3
    1       5
  0   2   4   6
       3 
         

*/