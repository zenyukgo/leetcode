func romanToInt(s string) int {
    romans := map[string]int{
        "M":  1000, 
        "CM": 900, 
        "D":  500, 
        "CD": 400, 
        "C":  100, 
        "XC": 90, 
        "L":  50, 
        "XL": 40, 
        "X":  10, 
        "IX": 9, 
        "V":  5, 
        "IV": 4, 
        "I":  1,
    }
    result := 0
    for len(s) > 1 {
        value, ok := romans[s[:2]]
        if ok {
            result += value
            s = s[2:]
        } else {
            value, ok := romans[s[:1]]
            if ok {
                result += value
                s = s[1:]
            } else {
                return -1
            }
        }
    }
    if len(s) == 1 {
        value, ok := romans[s[0:1]]
        if ok {
            result += value
            s = s[1:]
        } else {
            return -1
        }
    }
    return result
}