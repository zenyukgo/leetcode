func Abs(i int) int {
    if i < 0 {
        return -i
    }
    return i
}

func threeSumClosest(nums []int, target int) int {
    sort.Ints(nums)
    closest := nums[0] + nums[1] + nums[2]
    for i := 0; i < len(nums) - 2; i++ {
        l := i + 1
        r := len(nums) - 1
        for l < r {
            s := nums[i] + nums[l] + nums[r]
            if s == target {
                return s
            }
            if Abs(target - s) < Abs(target - closest) {
                closest = s
            } 
            if s < target {
                l++
            } else {
                r--
            }
        }
    }
    return closest
}