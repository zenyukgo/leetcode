func isPalindrome(x int) bool {
    if x < 10 && x >= 0 {
        return true
    }
    if x < 0 || x % 10 == 0 {
        return false
    }
    revertedSecondHalf := 0
    for x > revertedSecondHalf {
        revertedSecondHalf = revertedSecondHalf * 10 + x % 10
        x /= 10
    }
    if x == revertedSecondHalf {
        return true
    }
    revertedSecondHalf /= 10
    if x == revertedSecondHalf {
        return true
    }
    return false
}