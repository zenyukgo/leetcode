package main

import (
	"fmt"
)

func main() {
	fmt.Println("Hello, playground")
	
	l := lengthOfLongestSubstring("aabaab!bb")
	
	fmt.Printf("%v", l)
}

func lengthOfLongestSubstring(s string) int {
    current := []rune{}
    var previous []rune
    chars := []rune(s)
    
    end := 0
    for end < len(chars) {
	    exists := false
        for _, c := range current {
            if c == chars[end] {
                exists = true
                break
            }
	    } 
        if !exists {
            // extend window
            current = append(current, chars[end])
        } else {
            // slide window
            if len(current) > len(previous) {
                previous = make([]rune, len(current))
                copy(previous, current)
    	    }
	        current = shrinkToUnique(current, chars[end])
        }
        end++ 
    }
    if len(current) > len(previous) {
        previous = current
    }
    return len(previous)
}

func shrinkToUnique(current []rune, r rune) (result []rune){
	result = current
	indexOfRepeatedRune := -1
	for i, c := range current {
		if c == r {
			indexOfRepeatedRune = i
			break
		}
	}
	if indexOfRepeatedRune != -1 {
		result = append(current[indexOfRepeatedRune+1:], r)
	}
	return
}