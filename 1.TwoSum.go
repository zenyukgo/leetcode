// complexities: time - O(n2), space - O(1)
func twoSum(nums []int, target int) []int {
    for i := 0; i < len(nums); i++ {
        for j := i + 1; j < len(nums); j++ {
            if nums[i] + nums[j] == target {
                return []int{i, j}
            }
        }
    }
    return []int{}
}

// complexities: time - O(n); space - O(n)
func twoSum(nums []int, target int) []int {
    // key is value in the original array, value is the index in the original array
    all := map[int]int{}
    // need to have duplicates as map can't have multiple entries with same key
    // interested only in first 2 for case when 'target' == 2 * 'duplicated value'
    duplicates := map[int][2]int{}
    for idx, n := range nums {
        i, ok := all[n]
        if ok {
            _, dupAlreadyWritten := duplicates[n]
            if !dupAlreadyWritten {
                duplicates[n] = [2]int{i, idx}
            }
        }
        all[n] = idx
    }
    complement := 0
    for value, idx := range all {
        complement = target - value
        complementIndex, ok := all[complement]
        if ok {
            if complementIndex != idx {
                return []int{idx, complementIndex}
            } else if complement == value {
                _, dupExists := duplicates[value]
                if dupExists {
                    return []int{duplicates[value][0], duplicates[value][1]}
                }
            }
        }
    }
    return []int{}
}