package problem10

import (
	"fmt"
)

func isMatch(s string, p string) bool {
	pattern := findStarredChars(p)
	for _, r := range pattern {
		fmt.Printf("%v", r)
	}

	patternIndex := 0
	patternLen := len(pattern)
	skipCount := 0
	for i, r := range s {
		println(string(r))
		if skipCount > 0 {
			skipCount--
			continue
		}
		if patternIndex >= patternLen {
			return false
		}
		patternChar := pattern[patternIndex]
		if patternChar.anyChar && patternChar.repeated {
			// get first match after
			patternIndex++
			if patternIndex >= patternLen {
				// went all the way to the end of the any-match '.*' pattern
				return true
			}
			patternChar := pattern[patternIndex]
			skipCount = skipUntilMatch(patternChar, s[i:])
		}
		if patternChar.anyChar {
			patternIndex++
			continue
		}
		if patternChar.char == r && patternChar.repeated {
			continue
		}
		if patternChar.char == r {
			var canContinue bool
			canContinue, patternIndex = processSingleCharMatch(s, i, patternIndex, patternLen)
			if !canContinue {
				return false
			}
			continue
		}
		if patternChar.repeated {
			var canContinue bool
			isLastChar := i == len(s)-1
			canContinue, patternIndex = peekOneForward(r, isLastChar, patternIndex, patternLen, pattern)
			if !canContinue {
				return false
			}
		}
	}
	return patternIndex == patternLen-1
}

func processSingleCharMatch(s string, i, patternIndex, patternLen int) (canContinue bool, patternIndexOut int){
	if patternIndex+1 < patternLen {
		// not last symbol in pattern
		patternIndex++
		if i == len(s)-1 {
			// string has finished, but pattern expects continuation
			return false, patternIndex
		}
	} else if i < len(s)-1 {
		// pattern has finished, but string has not
		return false, patternIndex
	}
	return true, patternIndex
}

func peekOneForward(currentChar rune, isLastChar bool, patternIndex int, patternLen int, pattern []RepeatedChar) (canContinue bool, patternIndexOut int) {
	if patternIndex+1 < patternLen {
		peekOneForward := pattern[patternIndex+1]
		if peekOneForward.char == currentChar || peekOneForward.anyChar {
			if patternIndex+2 == patternLen {
				// last symbol in pattern
				return isLastChar, patternIndex+1
			}
			if peekOneForward.repeated {
				return true, patternIndex+1
			}
			return true, patternIndex+2
		} else {
			return false, patternIndex
		}
	} else {
		return false, patternIndex
	}
}

func skipUntilMatch(p RepeatedChar, s string) (skipCount int) {
	for _, r := range s {
		if p.char == r {
			return
		}
		skipCount++
	}
	return
}

func findStarredChars(pattern string) (result []RepeatedChar) {
	var previous RepeatedChar
	for i, r := range []rune(pattern) {
		if r == '*' && i == 0 {
			continue
		}
		if r == '.' {
			previous = RepeatedChar{true, '.', false}
			result = append(result, previous)
			continue
		}
		if r == '*' {
			previous.repeated = true
			result[len(result)-1] = previous
			continue
		}
		if previous.repeated && previous.char == r {
			// skipping as we already have this char in sequence
			// merge repeated sequence with the same single following char, e.g. "a*a" -> "a*"
			continue
		}
		previous = RepeatedChar{false, r, false}
		result = append(result, previous)
	}
	return
}
