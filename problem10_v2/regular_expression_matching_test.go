package problem10

import (
	"testing"
)

func TestIsMatch1(t *testing.T) {
	str := "aa"
	pattern := "a"
	expectedOutput := false

	result := isMatch(str, pattern)

	if result != expectedOutput {
		t.Fail()
	}
}

func TestIsMatch2(t *testing.T) {
	str := "aa"
	pattern := "a*"
	expectedOutput := true

	result := isMatch(str, pattern)

	if result != expectedOutput {
		t.Fail()
	}
}

func TestIsMatch3(t *testing.T) {
	str := "ab"
	pattern := ".*"
	expectedOutput := true

	result := isMatch(str, pattern)

	if result != expectedOutput {
		t.Fail()
	}
}

func TestIsMatch4(t *testing.T) {
	str := "aab"
	pattern := "c*a*b"
	expectedOutput := true

	result := isMatch(str, pattern)

	if result != expectedOutput {
		t.Fail()
	}
}

func TestIsMatch5(t *testing.T) {
	str := "mississippi"
	pattern := "mis*is*p*."
	expectedOutput := false

	result := isMatch(str, pattern)

	if result != expectedOutput {
		t.Fail()
	}
}

func TestIsMatch6(t *testing.T) {
	str := "mississippi"
	pattern := "mis*is*ip*."
	expectedOutput := true

	result := isMatch(str, pattern)

	if result != expectedOutput {
		t.Fail()
	}
}

func TestIsMatch7(t *testing.T) {
	str := "aa"
	pattern := "aa"
	expectedOutput := true

	result := isMatch(str, pattern)

	if result != expectedOutput {
		t.Fail()
	}
}

func TestIsMatch8(t *testing.T) {
	str := "abcd"
	pattern := "d*"
	expectedOutput := false

	result := isMatch(str, pattern)

	if result != expectedOutput {
		t.Fail()
	}
}

func TestIsMatch9(t *testing.T) {
	str := "aaa"
	pattern := "aaaa"
	expectedOutput := false

	result := isMatch(str, pattern)

	if result != expectedOutput {
		t.Fail()
	}
}

func TestIsMatch10(t *testing.T) {
	str := "aaa"
	pattern := "a*a"
	expectedOutput := true

	result := isMatch(str, pattern)

	if result != expectedOutput {
		t.Fail()
	}
}

func TestIsMatch11(t *testing.T) {
	str := "aaa"
	pattern := "ab*a"
	expectedOutput := false

	result := isMatch(str, pattern)

	if result != expectedOutput {
		t.Fail()
	}
}

func TestIsMatch12(t *testing.T) {
	str := "aa"
	pattern := "b*a"
	expectedOutput := false

	result := isMatch(str, pattern)

	if result != expectedOutput {
		t.Fail()
	}
}

func TestIsMatch13(t *testing.T) {
	str := "aaa"
	pattern := "ab*a*c*a"
	expectedOutput := true

	result := isMatch(str, pattern)

	if result != expectedOutput {
		t.Fail()
	}
}

func TestIsMatch14(t *testing.T) {
	str := "abcdede"
	pattern := "ab.*de"
	expectedOutput := true

	result := isMatch(str, pattern)

	if result != expectedOutput {
		t.Fail()
	}
}
