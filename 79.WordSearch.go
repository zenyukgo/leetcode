
func exist(board [][]byte, word string) bool {
    for _, coords := range findStarts(board, rune(word[0])) {
        if len(word) == 1 {
            return true
        }
        found := find(board, word[1:], coords, []xy{coords})
        if found {
            return true
        }
    }
    return false
}

func find(board [][]byte, word string, from xy, visited []xy) bool {
    notVisitedNeig := []xy{}
Outside:
    for _, n := range getNeighbours(board, from) {
        for _, v := range visited {
            if n.x == v.x && n.y == v.y {
		continue Outside
            }
        }
        notVisitedNeig = append(notVisitedNeig, n)
    }
    for _, n := range notVisitedNeig {
        if board[n.x][n.y] == byte(word[0]) {
            if len(word) == 1 {
                return true
            }
            visited = append(visited, n)
            found := find(board, word[1:], n, visited)
            if found {
                return true
            } else {
                visited = visited[:len(visited)-1]
            }
        }
    }
    return false
}

func findStarts(board [][]byte, r rune) []xy {
    result := []xy{}
    b := byte(r)
    for i := 0; i < len(board); i++ {
        for j := 0; j < len(board[0]); j++ {
            if board[i][j] == b {
                result = append(result, xy{
                    x: byte(i),
                    y: byte(j),
                })
            }
        }
    }
    return result
}

func getNeighbours(board [][]byte, pos xy) []xy {
    result := []xy{}
    // top
    if pos.x > 0 {
        result = append(result, xy{
            x: pos.x - 1,
            y: pos.y,
        })
    }
    // bottom
    if int(pos.x) + 1 < len(board) {
        result = append(result, xy{
            x: pos.x + 1,
            y: pos.y,
        })
    }
    // left
    if pos.y > 0 {
        result = append(result, xy{
            x: pos.x,
            y: pos.y - 1,
        })
    }
    // right
    if int(pos.y) + 1 < len(board[0]) {
        result = append(result, xy{
            x: pos.x,
            y: pos.y + 1,
        })
    }
    return result
}

type xy struct {
    x byte
    y byte
}