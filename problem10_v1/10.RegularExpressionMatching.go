package problem10_v1

import (
	"fmt"
	"strings"
)

func main() {
	println(isMatch("cdede", "c*de"))
}
func isMatch(s string, p string) bool {    
    fmt.Printf("-------------------\ns=%v %v p=%v %v\n", s, len(s), p, len(p))

    if len(p) == 0 {
        return len(s) == 0
    }
    if len(s) == 0 {
	if len(p) >= 2 && p[1] == '*' {
	    return isMatch(s, p[2:])
	}
        return false
    }
    if len(p) >= 2 && p[:2] == ".*" {
        if len(p) == 2 {
            return true
        } 
        return isMatch(s, p[2:]) || isMatch(s, s[0:1] + p[1:])
    }
    if len(p) >= 2 && p[1] == '*' {
        ok, substringIdx := sequenceMatch(s, rune(p[0]))
        repeated := repeatedCharCount(p[2:], rune(p[0]))
        
    	if substringIdx > 0 {
    		substringIdx -= repeated
    	}
            if isMatch(s, p[2:]) || (ok && isMatch(s[substringIdx+1:], p) ) {
    		return true
    	}
    	newP := p
        for len(newP[2:]) >=2 {
	    idx :=  strings.Index(newP[2:], "*")
	    if idx > -1 {
	        sub := []rune(newP)
	        newPattern := string(append(sub[:idx+1], sub[idx+3:]...))
	        if isMatch(s, newPattern) {
			return true
		} 
	    }
	    newP = newP[2:]
	    break
	}

        return false
    }
    if s[0] == p[0] || p[0] == '.' {
        return isMatch(s[1:], p[1:])
    } else {
        return false
    }
}

func sequenceMatch(s string, p rune) (bool, int) {
    lastIndex := 0
    matched := false
    for i := 0; i < len(s); i++ {
        if rune(s[i]) == p {
            lastIndex = i
	    matched = true
            continue
        }
        break
    }
    return matched, lastIndex
} 

func repeatedCharCount(s string, r rune) int {
    result := 0
    for _, ch := range s {
        if ch == r {
            result++
            continue
        }
        break
    }
    return result
}