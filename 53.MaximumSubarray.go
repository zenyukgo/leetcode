func maxSubArray(nums []int) int {
    result := nums[0]
    total := 0
    min := 0
    for _, value := range nums {
        total += value
        if total - min > result {
            result = total - min    
        }
        if total < min {
            min = total
        }
    }   
    return result
}