func merge(nums1 []int, m int, nums2 []int, n int)  {
    i1 := len(nums1) - len(nums2) - 1
    if i1 < 0 {
        for i := 0; i < len(nums1); i++ {
            nums1[i] = nums2[i]
        }
        return
    }
    i1end := len(nums1) - 1
    i2 := len(nums2) - 1
    for len(nums2) > 0 {
        if i1 < 0 {
            idx := 0
            for idx < len(nums2) {
                nums1[idx] = nums2[idx]
                idx++
            } 
            return
        }
        if nums2[i2] > nums1[i1] {
            nums1[i1end] = nums2[i2]
            nums2 = nums2[:i2]
            i2--
        } else {
            nums1[i1end] = nums1[i1]
            nums1[i1] = 0
            i1--
        }
        i1end--
    }   
}