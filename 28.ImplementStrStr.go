func strStr(haystack string, needle string) int {
    if len(needle) == 0 {
        return 0
    }
    if len(haystack) == 0 || len(needle) > len(haystack) {
        return -1
    }
    if haystack[:len(needle)] == needle {
        return 0
    }
    index := 0
    for index + len(needle) <= len(haystack) {
        if haystack[index] == needle[0] {
            if haystack[index:index+len(needle)] == needle {
                return index
            }
        }
        index++
    }
    return -1
}